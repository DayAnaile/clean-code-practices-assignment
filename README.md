# Clean Code Practices Assignment

A small application that presented best practices and design principles applied.

# SOLID principles

<p>
    S - Single-responsiblity Principle <br />
    O - Open-closed Principle <br />
    L - Liskov Substitution Principle <br />
    I - Interface Segregation Principle <br />
    D - Dependency Inversion Principle <br />
</p>

# DRY, KISS, YAGNI

**DRY** 
<p> 
    DON’T REPEAT YOURSELF
</p>

**KISS**
<p>
    KEEP IT SHORT AND SIMPLE
</p>
    
**YAGNI**
<p>
    YOU AIN'T GONNA NEED IT
</p>

# Package by Feature

<p>
    Package-by-feature uses packages to reflect the feature set. It tries to place all items related to a single feature (and only that feature) into a single directory/package. It is highly recommended this method due to scalability and loose coupling.
</p>
<p>
    [Enterprise Application Project Structure For Spring Boot 2.*](https://medium.com/@ijayakantha/enterprise-application-project-structure-spring-boot-2-7a7186bd0f66)
</p>
